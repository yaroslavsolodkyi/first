from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.sign_up, name='sign_up'),
    url(r'^success/$', views.success_registration, name='success'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'},
        name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^email/$', views.EmailView.as_view(), name='email'),

]
