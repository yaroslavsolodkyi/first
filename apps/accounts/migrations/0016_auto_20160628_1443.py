# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-28 14:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_auto_20160628_1312'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='first_name',
            field=models.CharField(max_length=30, null=True, verbose_name='first name'),
        ),
        migrations.AlterField(
            model_name='account',
            name='last_name',
            field=models.CharField(max_length=30, null=True, verbose_name='last name'),
        ),
        migrations.AlterField(
            model_name='account',
            name='mobile',
            field=models.IntegerField(null=True),
        ),
    ]
