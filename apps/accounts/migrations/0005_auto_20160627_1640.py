# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-27 16:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20160627_1639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='mobile',
            field=models.IntegerField(),
        ),
    ]
