from django import forms

from .models import Account


class AccountForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password1'}),
                                label="Password", required=False)
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password2'}),
                                label="Password (again)", required=False)

    class Meta:
        model = Account
        fields = [
            'first_name', 'last_name', 'email', 'mobile',
            'univer', 'remarks', 'pupil', 'password1', 'password2'
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'mobile': forms.NumberInput(attrs={'class': 'form-control'}),
            'univer': forms.Select(attrs={'class': 'form-control',
                                          'id': 'univer_lector'}),
            'remarks': forms.Textarea(attrs={'class': 'form-control'}),
            'pupil': forms.CheckboxInput()
        }
        labels = {
            'first_name': 'First name',
            'last_name': 'Last name',
            'email': 'Email',
            'mobile': 'Phone number',
            'univer': 'University',
            'remarks': 'Remarks',
        }

    def clean(self):
        """
        Verifies that the values entered into the password fields match
        NOTE: Errors here will appear in ``non_field_errors()`` because it
        applies to more than one field.
        """

        cleaned_data = super(AccountForm, self).clean()
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError("Passwords don't match." +
                                            "Please enter both fields again.")
        return self.cleaned_data

    def save(self, commit=True):
        user = super(AccountForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class EmailForm(forms.Form):
    email = forms.EmailField()

class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Email Or Username"), max_length=254)
    # def send_email(self):
    #     current_site = Site.objects.get_current()
    #     protocol = getattr(settings, "DEFAULT_HTTP_PROTOCOL", "http")

    #     subject = "".join(subject.splitlines())

    #     context = {
    #         'guide': guide_status.guide,
    #         'group': guide_status.group,
    #         'booking': guide_status.booking
    #     }

    #     body_html = get_template(
    #         'booking/emails/optional/guide_email.html').render(context)
    #     send_mail(
    #         subject=subject,
    #         message='',
    #         html_message=body_html,
    #         from_email=settings.DEFAULT_FROM_EMAIL,
    #         recipient_list=[guide_status.guide.user.email]
    #     )
    #     pass
