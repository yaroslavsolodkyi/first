from .models import Account


class CustomBackend(object):

    def authenticate(self, username=None, password=None):
        try:
            user = Account.objects.get(email=username)

            if user.check_password(password):
                return user
            else:
                return None
        except Account.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Account.objects.get(pk=user_id)
        except Account.DoesNotExist:
            return None
