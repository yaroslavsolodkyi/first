from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView

from apps.lector.forms import LectorForm
from apps.utils.views import render_to

from .forms import AccountForm, EmailForm


def custom_404(request):
    return render_to_response('404.html')


@render_to('register-success.html')
def success_registration(request):
    return {}


@render_to('home.html')
def home(request):
    return {'request': request, 'user': request.user}


@render_to('registration-form.html')
def sign_up(request):
    if request.method == 'POST':
        account_form = AccountForm(request.POST)
        lector_form = LectorForm(request.POST)

        if account_form.is_valid():
            account = account_form.save(commit=False)
            if lector_form.is_valid():
                account.save()
                lector = lector_form.save()
                account.lector = lector
                account.save()
            else:
                account.pupil = True
                account.save()

            return HttpResponseRedirect(reverse('university:overview'))
    else:
        account_form = AccountForm()
        lector_form = LectorForm()

    return {'account_form': account_form, 'lector_form': lector_form}


class EmailView(FormView):
    template_name = "email_page.html"
    form_class = EmailForm
    success_url = reverse_lazy('accounts:sign_up')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_email()
        return super(EmailView, self).form_valid(form)


# def register(request):
#     if request.method == 'POST':
#         captcha_error = ""
#         captcha_response = captcha.submit(
#                 request.POST.get("recaptcha_challenge_field", None),
#                 request.POST.get("recaptcha_response_field", None),
#                 settings.RECAPTCHA_PRIVATE_KEY,
#                 request.META.get("REMOTE_ADDR", None))
#         if not captcha_response.is_valid:
#             captcha_error = "&error=%s" % captcha_response.error_code
#             c = {}
#             c.update(csrf(request))
#             c['repetir'] = True
#             c['header'] = "register"
#             return render_to_response('registration-form.html', c,
#                                       context_instance=RequestContext(request))
#         else:
#             if error_register(request):
#                 c = {}
#                 c.update(csrf(request))
#                 c['repetir'] = True
#                 c['header'] = "register"
#                 return render_to_response('registration-form.html', c,
#                                           context_instance=RequestContext(request))
#             else:
#                 username = clean_username(request.POST['user'])
#                 password = request.POST['password']
#                 email = request.POST['email']
#                 user = User.objects.create_user(username, email, password)
#                 user.is_active = False
#                 user.save()
#                 confirmation_code = ''.join(tehrandom.choice(string.ascii_uppercase +
#                                                              string.digits +
#                                                              string.ascii_lowercase)
#                                             for x in range(33))
#                 p = Profile(user=user, confirmation_code=confirmation_code)
#                 p.save()
#                 send_registration_confirmation(user)
#                 return HttpResponseRedirect('../../../../../')
#     else:
#         c = create_c(request)
#         c['header'] = "register"
#         return render_to_response('registration-form.html', c,
#                                   context_instance=RequestContext(request))
#
#
# def send_registration_confirmation(user):
#     p = user.get_profile()
#     title = "Gsick account confirmation"
#     content = "http://www.gsick.com/confirm/" + str(p.confirmation_code) + "/" + user.username
#     send_mail(title, content, 'no-reply@gsick.com', [user.email], fail_silently=False)
#
#
# def confirm(request, confirmation_code, username):
#     try:
#         user = User.objects.get(username=username)
#         profile = user.get_profile()
#         if profile.confirmation_code == confirmation_code and user.date_joined >
#         (datetime.datetime.now()-datetime.timedelta(days=1)):
#             user.is_active = True
#             user.save()
#             user.backend = 'django.contrib.auth.backends.ModelBackend'
#             auth_login(request, user)
#         return HttpResponseRedirect('../../../../../')
#     except:
#         return HttpResponseRedirect('../../../../../')
#
#
# def error_register(request):
#     first_name = request.POST['first_name']
#     password1 = request.POST['password1']
#     email = request.POST['email']
#     if not clean_username(username):
#         return True
#     if username.replace(" ", "") == "" or password.replace(" ", "") == "":
#         return True
#     if len(username) > 15 or len(password) > 50:
#         return True
#     if "@" not in email:
#         return True
#     try:
#         if User.objects.get(username=username):
#             return True
#     except:
#         pass
