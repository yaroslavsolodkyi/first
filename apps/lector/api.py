from django.shortcuts import get_object_or_404

from apps.accounts.models import Account
from apps.university.models import Subject
from apps.utils.views import render_json

from .models import Lector


@render_json
def lector_delete(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            lector = get_object_or_404(Account, pk=request.POST.get('lector_id'))
            lector.delete()
            return {'success': True}
        except Account.DoesNotExist:
            pass
        except ValueError:
            pass
    return {'success': False}


@render_json
def add_subject(request):
    if request.is_ajax() and request.method == 'GET':
        subjects = Subject.objects.filter(univer=request.GET.get('university'))
        if subjects:
            subjects_data = [(subject.id, subject.title) for subject in subjects]
            return {'success': True,
                    'data': {'subjects': subjects_data}
                    }
    return {'success': False}


@render_json
def edit_subject(request):
    if request.is_ajax() and request.method == 'GET':
        subjects = Subject.objects.filter(univer=request.GET.get('university'))
        lector = Lector.objects.get(pk=request.GET.get('lector_id'))
        if subjects:
            for subject in subjects:
                if subject in lector.subject.all():
                    subjects_data = (subject.id, subject.title, True)
                else:
                    subjects_data = (subject.id, subject.title, False)

            return {'success': True,
                    'data': {'subjects': subjects_data}
                    }
    return {'success': False}
