from django.conf.urls import url

from . import api, views

urlpatterns = [
    url(r'^$', views.lector_overview, name='overview'),
    url(r'^add/$', views.lector_add, name='add'),
    url(r'^add_subject/$', api.add_subject, name='add_subject'),
    url(r'^edit/(?P<id>[0-9]+)$', views.lector_edit, name='edit'),
    url(r'^delete/$', api.lector_delete, name='delete'),
    url(r'^calendar/$', views.calendar, name='calendar'),
    url(r'^get_events$', views.get_events, name='get_events'),
    url(r'^set_events$', views.set_events, name='set_events'),
    url(r'^resize_events$', views.resize_events, name='resize_events'),
    url(r'^edit_events$', views.edit_events, name='edit_events'),
]
