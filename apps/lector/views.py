import json
from datetime import datetime, timedelta

import timestring
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from apps.accounts.forms import AccountForm
from apps.accounts.models import Account
from apps.utils.views import render_json, render_to

from .forms import LectorForm
from .models import Event


@render_to('lector-overview.html')
def lector_overview(request):
    all_lector = Account.objects.all().filter(lector__isnull=False)
    if 'query_lector' in request.GET:
        lectors = all_lector.objects.filter(
            first_name__contains=request.GET.get('query_lector'))
    else:
        lectors = all_lector
    return {'lectors': lectors}


@render_to('lector-add.html')
def lector_add(request):
    if request.method == 'POST':
        account_form = AccountForm(request.POST)
        lector_form = LectorForm(request.POST)
        if lector_form.is_valid() and account_form.is_valid():
            account = account_form.save()
            lector = lector_form.save(commit=False)
            account.lector = lector
            account.save()

            return HttpResponseRedirect(reverse('lector:overview'))
    else:
        lector_form = LectorForm()
        account_form = AccountForm()

    return {'lector_form': lector_form, 'account_form': account_form}


@render_to('lector-edit.html')
def lector_edit(request, id):
    lector = get_object_or_404(Account, pk=id)
    lector_form = AccountForm(instance=lector)

    if request.method == 'POST':
        lector_form = AccountForm(request.POST, instance=lector)
        lector = lector_form.save(commit=False)
        if lector_form.is_valid():
            lector_form.save()
            return HttpResponseRedirect(reverse('lector:overview'))

    return {'lector_form': lector_form}


@csrf_exempt
def get_events(request):
    events = Event.objects.all()
    event_list = []
    for event in events:
        event_start = event.start.astimezone(timezone.get_default_timezone())
        event_end = event.end.astimezone(timezone.get_default_timezone())
        if event_start.hour == 0 and event_start.minute == 0:
            allDay = True
        else:
            allDay = False
        if not event.is_cancelled:
            event_list.append({
                'id': event.id,
                'start': event_start.strftime('%Y-%m-%d %H:%M:%S'),
                'end': event_end.strftime('%Y-%m-%d %H:%M:%S'),
                'title': event.title,
                'allDay': allDay
            })
    if len(event_list) == 0:
        raise Http404
    else:
        return HttpResponse(json.dumps(event_list),
                            content_type='application/json')


@csrf_exempt
@render_json
def set_events(request):
    if request.method == 'POST' and request.is_ajax():
        start = str(timestring.Date(request.POST.get('start')))
        end = str(timestring.Date(request.POST.get('end')))
        event = Event.objects.create(id=request.POST.get('event_id'),
                                     title=request.POST.get('title'),
                                     start=start,
                                     end=end)
        event_data = (event.id, event.title, start, end)
        return {'success': True, 'data': {'event_data': event_data}}
    return {'success': False}


@csrf_exempt
@render_json
def resize_events(request):
    if request.is_ajax() and request.method == 'POST':
        new = datetime.strptime(request.POST.get(
            'end'), "%Y-%m-%d %H:%M:%S") + timedelta(milliseconds=int(request.POST.get('delta')))

        event = Event.objects.filter(id=request.POST.get('event_id'))[0]
        event.end = new
        event.save()
        return {'success': True, 'data': {'event_id': event.id}}
    return {'success': False}


@csrf_exempt
@render_json
def edit_events(request):
    if request.is_ajax() and request.method == 'POST':
        if request.POST.get('status') == 'delete':
            event = Event.objects.filter(id=request.POST.get('event_id'))[0]
            event.delete()
        elif request.POST.get('status') == 'edit':
            event = Event.objects.filter(id=request.POST.get('event_id'))[0]
            event.title = request.POST.get('title')
            event.save()
        return {'success': True, 'data': {'event_id': event.id}}
    return {'success': False}


@render_to('calendar.html')
def calendar(request):
    return {}
