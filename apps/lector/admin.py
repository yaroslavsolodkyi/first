from django.contrib import admin

from .models import Event, Lector

admin.site.register(Lector)
admin.site.register(Event)
