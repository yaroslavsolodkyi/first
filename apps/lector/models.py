from django.db import models


class Lector(models.Model):
    subject = models.ManyToManyField('university.Subject')

    def __str__(self):
        return '{}'.format(self.id)


class Event(models.Model):
    title = models.TextField(blank=True)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    allDay = models.BooleanField(default=False)
    lector = models.ForeignKey(Lector, null=True)
    is_cancelled = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.title)
