from django import forms
from django.forms import ModelForm

from .models import Lector


class LectorForm(ModelForm):
    class Meta:
        model = Lector
        fields = ['subject']
        widgets = {'subject': forms.CheckboxSelectMultiple()}
