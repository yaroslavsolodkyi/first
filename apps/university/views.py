from django.core.urlresolvers import reverse
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

from apps.utils.views import render_to

from .forms import ContactForm, SubjectForm, UniversityForm
from .models import Contact, Subject, University


@render_to('university-overview.html')
def university_overview(request):
    if 'query_univ' in request.GET:
        universities = University.objects.filter(
            title__contains=request.GET.get('query_univ'))
    else:
        universities = University.objects.all()
    return {'universities': universities}


@render_to('university-add.html')
def university_add(request):
    ContactFormSet = inlineformset_factory(
        University, Contact, extra=1, form=ContactForm)
    SubjectFormSet = inlineformset_factory(
        University, Subject, extra=1, form=SubjectForm)
    if request.method == 'POST':
        university_form = UniversityForm(request.POST)
        contact_formset = ContactFormSet(request.POST)
        subject_formset = SubjectFormSet(request.POST)
        if (university_form.is_valid() and contact_formset.is_valid() and
                subject_formset.is_valid()):
            instance = university_form.save()
            contact_formset.instance = instance
            subject_formset.instance = instance
            contact_formset.save()
            subject_formset.save()
            return HttpResponseRedirect(reverse('university:overview'))
    else:
        contact_formset = ContactFormSet(queryset=Contact.objects.none())
        subject_formset = SubjectFormSet(queryset=Subject.objects.none())
        university_form = UniversityForm()

    return {'university_form': university_form, 'contact_formset': contact_formset,
            'subject_formset': subject_formset}


@render_to('university-edit.html')
def university_edit(request, id):
    university = get_object_or_404(University, pk=id)
    university_form = UniversityForm(instance=university)
    ContactFormSet = inlineformset_factory(
        University, Contact, extra=0, form=ContactForm)
    contact_formset = ContactFormSet(instance=university)
    SubjectFormSet = inlineformset_factory(
        University, Subject, extra=0, form=SubjectForm)
    subject_formset = SubjectFormSet(instance=university)

    if request.method == 'POST':
        university_form = UniversityForm(request.POST, instance=university)
        university = university_form.save(commit=False)
        contact_formset = ContactFormSet(request.POST, instance=university)
        subject_formset = SubjectFormSet(request.POST, instance=university)
        if (university_form.is_valid() and contact_formset.is_valid() and
                subject_formset.is_valid()):
            university_form.save()
            contact_formset.save()
            subject_formset.save()
            return HttpResponseRedirect(reverse('university:overview'))

    return {'university_form': university_form, 'contact_formset': contact_formset,
            'subject_formset': subject_formset}


def university_delete(request, id):
    instance = get_object_or_404(University, pk=id)
    Contact.objects.filter(unversity=instance).delete()
    University.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('university:overview'))


def contact_delete(request, id):
    Contact.objects.get(pk=id).delete()
    return HttpResponseRedirect(reverse('university:overview'))
