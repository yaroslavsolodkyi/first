from django.contrib import admin

from .models import Contact, Subject, University

admin.site.register(Subject)
admin.site.register(Contact)
admin.site.register(University)
