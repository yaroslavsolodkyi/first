from django.db import models


class University(models.Model):
    title = models.CharField(max_length=30)
    country = models.CharField(max_length=30)
    city = models.CharField(max_length=60)
    address = models.CharField(max_length=30)
    fax = models.IntegerField(blank=True, null=True)
    phone = models.IntegerField(blank=True, null=True)
    glob_email = models.EmailField(max_length=254)
    num_stud = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "universities"

    def __str__(self):
        return '{} {}'.format(self.title, self.city)


class Contact(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    mobile = models.IntegerField(blank=True, null=True)
    email = models.EmailField(max_length=254)
    remarks = models.CharField(max_length=600, blank=True)
    univer = models.ForeignKey(University, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Subject(models.Model):
    title = models.CharField(max_length=30)
    hours = models.IntegerField(default=30)
    description = models.CharField(max_length=600, blank=True)
    univer = models.ForeignKey(University, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.title)
