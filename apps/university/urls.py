from django.conf.urls import url

from . import api, views

urlpatterns = [
    url(r'^$', views.university_overview, name='overview'),
    url(r'^add/$', views.university_add, name='add'),
    url(r'^edit/(?P<id>[0-9]+)$', views.university_edit, name='edit'),
    url(r'^delete/$', api.university_delete, name='delete'),
    ]
