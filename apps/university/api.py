from django.shortcuts import get_object_or_404

from apps.utils.views import render_json

from .models import University


@render_json
def university_delete(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            university = get_object_or_404(University, pk=request.POST.get('university_id'))
            university.delete()
            return {'success': True}
        except University.DoesNotExist:
            pass
        except ValueError:
            pass
    return {'success': False}
