from django import forms
from django.forms import ModelForm

from .models import Contact, Subject, University


class UniversityForm(ModelForm):
    class Meta:
        model = University
        fields = ['title', 'country', 'city', 'address',
                  'fax', 'phone', 'glob_email', 'num_stud']
        widgets = {'title': forms.TextInput(attrs={'class': 'form-control'}),
                   'country': forms.TextInput(attrs={'class': 'form-control'}),
                   'city': forms.TextInput(attrs={'class': 'form-control'}),
                   'address': forms.TextInput(attrs={'class': 'form-control'}),
                   'fax': forms.NumberInput(attrs={'class': 'form-control'}),
                   'phone': forms.NumberInput(attrs={'class': 'form-control'}),
                   'glob_email': forms.EmailInput(attrs={'class': 'form-control'}),
                   'num_stud': forms.NumberInput(attrs={'class': 'form-control'})
                   }
        labels = {
            'title': ('Title'),
            'country': ('Country'),
            'city': ('City'),
            'address': ('Address'),
            'fax': ('Fax'),
            'phone': ('Phone'),
            'num_stud': ('Total number of students'),
            'glob_email': ('Global email')
            }


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'mobile', 'email', 'remarks']
        widgets = {'first_name': forms.TextInput(attrs={'class': 'form-control'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control'}),
                   'mobile': forms.NumberInput(attrs={'class': 'form-control'}),
                   'email': forms.EmailInput(attrs={'class': 'form-control'}),
                   'remarks': forms.Textarea(attrs={'class': 'form-control'})
                   }
        labels = {
            'first_name': ('First name'),
            'last_name': ('Last name'),
            'mobile': ('Mobile'),
            'email': ('Email'),
            'remarks': ('Remarks')
            }


class SubjectForm(ModelForm):
    class Meta:
        model = Subject
        fields = ['title', 'hours', 'description']
        widgets = {'title': forms.TextInput(attrs={'class': 'form-control'}),
                   'hours': forms.NumberInput(attrs={'class': 'form-control'}),
                   'description': forms.Textarea(attrs={'class': 'form-control'})
                   }
        labels = {
            'title': ('Subject'),
            'hours': ('Hours'),
            'description': ('Description')
            }
