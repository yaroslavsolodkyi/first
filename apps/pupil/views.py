from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from formtools.wizard.views import SessionWizardView

from apps.accounts.forms import AccountForm
from apps.accounts.models import Account
from apps.utils.views import render_to

from .forms import GroupForm
from .models import Group


@render_to('pupil-overview.html')
def pupil_overview(request):

    all_pupil = Account.objects.all().filter(pupil=True)
    if 'q' in request.GET:
        pupils = all_pupil.objects.filter(
            first_name__contains=request.GET.get('q'))
    else:
        pupils = all_pupil
    return {'pupils': pupils}


@render_to('pupil-add.html')
def pupil_add(request):
    if request.method == 'POST':

        pupil_form = AccountForm(request.POST)
        if pupil_form.is_valid():
            # import ipdb; ipdb.set_trace()
            pupil_form.cleaned_data['pupil'] = 'checked'

            pupil_form.save()
            return HttpResponseRedirect(reverse('pupil:overview'))
    else:
        pupil_form = AccountForm()

    return {'pupil_form': pupil_form}


@render_to('pupil-edit.html')
def pupil_edit(request, id):
    pupil = get_object_or_404(Account, pk=id)
    pupil_form = AccountForm(instance=pupil)

    if request.method == 'POST':
        pupil_form = AccountForm(request.POST, instance=pupil)
        pupil = pupil_form.save(commit=False)
        if pupil_form.is_valid():
            pupil_form.save()
            return HttpResponseRedirect(reverse('pupil:overview'))

    return {'pupil_form': pupil_form}


class GroupWizard(SessionWizardView):
    template_name = 'group-form.html'

    def done(self, form_list, **kwargs):
        data = {}
        subjects = {}
        pupils = {}
        for form in form_list:
            if form.is_valid():
                data.update(form.cleaned_data)
        for key in data.keys():
            if key == 'subject':
                subjects[key] = data['subject']
            elif key == 'pupils':
                pupils[key] = data['pupils']
        del data['pupils']
        del data['subject']

        group = Group.objects.create(**data)
        for value in subjects['subject']:
            group.subject.add(value)
        for value in pupils['pupils']:
            group.pupils.add(value)
        group.save()
        return HttpResponseRedirect(reverse('pupil:group_success'))


@render_to('done.html')
def success_group_creation(request):
    return {}


@render_to('group-overview.html')
def group_overview(request):
    if 'query_group' in request.GET:
        groups = Group.objects.filter(
            first_name__contains=request.GET.get('query_group'))
    else:
        groups = Group.objects.all()
    return {'groups': groups}


@render_to('group-edit.html')
def group_edit(request, id):

    group = get_object_or_404(Group, pk=id)
    group_form = GroupForm(instance=group)

    if request.method == 'POST':
        group_form = GroupForm(request.POST, instance=group)
        if group_form.is_valid():
            group_form.save()
            return HttpResponseRedirect(reverse('pupil:group_overview'))

    return {'group_form': group_form}
