from django.conf.urls import url

from . import api, views
from .forms import GroupForm1, GroupForm2, GroupForm3

urlpatterns = [
    url(r'^$', views.pupil_overview, name='overview'),
    url(r'^add/$', views.pupil_add, name='add'),
    url(r'^add_subject/$', api.add_subject, name='add_subject'),
    url(r'^edit/(?P<id>\d+)$', views.pupil_edit, name='edit'),
    url(r'^delete/$', api.pupil_delete, name='delete'),
    url(r'^group/$', views.GroupWizard.as_view([GroupForm1, GroupForm2, GroupForm3]), name='group'),
    url(r'^group/done/$', views.success_group_creation, name='group_success'),
    url(r'^group/overview/$', views.group_overview, name='group_overview'),
    url(r'^group/edit/(?P<id>\d+)$', views.group_edit, name='group_edit'),
    url(r'^group/delete/$', api.group_delete, name='group_delete'),
    ]
