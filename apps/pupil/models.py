from django.db import models


class Group(models.Model):
    univer = models.ForeignKey('university.University')
    curator = models.ForeignKey('lector.Lector')
    subject = models.ManyToManyField('university.Subject')
    department = models.CharField(max_length=30)
    maximum = models.IntegerField(blank=True, null=True)
    minimum = models.IntegerField(blank=True, null=True)
    pupils = models.ManyToManyField('accounts.Account')

    def __str__(self):
        return '{} {}'.format(self.department, self.univer)
