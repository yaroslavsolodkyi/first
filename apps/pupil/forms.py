from django import forms
from django.forms import ModelForm

from .models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['univer', 'department', 'curator', 'subject', 'maximum',
                  'minimum', 'pupils']
        widgets = {
            'univer': forms.Select(attrs={'class': 'form-control', 'id': 'univer_pupil'}),
            'curator': forms.Select(attrs={'class': 'form-control'}),
            'department': forms.TextInput(attrs={'class': 'form-control'}),
            'subject': forms.CheckboxSelectMultiple(),
            'minimum': forms.NumberInput(attrs={'class': 'form-control'}),
            'maximum': forms.NumberInput(attrs={'class': 'form-control'}),
            'pupils': forms.SelectMultiple(),
            }
        labels = {
            'univer': 'University',
            'department': 'Department',
            'curator': 'Curator',
            'minimum': 'Minimum students number',
            'maximum': 'Maximum students number',
            'pupils': 'Students selection',
            }


class GroupForm1(ModelForm):
    class Meta:
        model = Group
        fields = ['univer', 'department', 'curator', 'subject']
        widgets = {
            'univer': forms.Select(attrs={'class': 'form-control', 'id': 'univer_pupil'}),
            'curator': forms.Select(attrs={'class': 'form-control'}),
            'department': forms.TextInput(attrs={'class': 'form-control'}),
            'subject': forms.CheckboxSelectMultiple(),
            }
        labels = {
            'univer': 'University',
            'department': 'Department',
            'curator': 'Curator',
            }


class GroupForm2(ModelForm):
    class Meta:
        model = Group
        fields = ['maximum', 'minimum']
        widgets = {
            'minimum': forms.NumberInput(attrs={'class': 'form-control'}),
            'maximum': forms.NumberInput(attrs={'class': 'form-control'}),
            }
        labels = {
            'minimum': 'Minimum students number',
            'maximum': 'Maximum students number',
            }


class GroupForm3(ModelForm):
    class Meta:
        model = Group
        fields = ['pupils']
        widgets = {
                'pupils': forms.SelectMultiple(attrs={'class': 'group_select',
                                                      'multiple': 'multiple'})
                }
        labels = {'pupils': 'Students selection'}
