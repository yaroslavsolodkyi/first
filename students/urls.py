"""students URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
admin.autodiscover()

urlpatterns = [
    url(r'^university/',
        include('apps.university.urls', namespace='university')),
    url(r'^lector/', include('apps.lector.urls', namespace='lector')),
    url(r'^pupil/', include('apps.pupil.urls', namespace='pupil')),
    url(r'^my_account/', include('apps.accounts.urls', namespace='accounts')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'apps.accounts.views.home', name='home'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^resetpassword/passwordsent$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^resetpassword/$', auth_views.password_reset, name="reset_password"),
    url(r'^reset(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done$', auth_views.password_reset_complete, name='password_reset_complete'),
]
